import { Component, OnInit, OnDestroy } from '@angular/core';
import { Country } from '../country.model';
import { Subscription, Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Store, select } from '@ngrx/store';
import * as CountryActions from '../store/country.actions';
import { getSelectedCountry, getAPILoaded, getLoading, getWishlist, getMessage } from '../store/index';

@Component({
  selector: 'app-country-details-container',
  template: `
    <app-country-details
      [selectedCountry]="selectedCountry$ | async"
      [loading]="loading$ | async"
      [message]="message$ | async"
      (countryIdEmitter)="addCountry($event)"
    >
  `
})
export class CountryDetailsContainerComponent implements OnInit, OnDestroy {

  public selectedCountry$: Observable<Country>;
  private selectedCountrySub: Subscription;
  private selectedCountry: Country;
  private id: string;
  private idSub: Subscription;
  public loading$: Observable<boolean>;
  private APILoaded$: Observable<boolean>;
  private APILoadedSub: Subscription;
  private APILoaded: boolean;
  public message$: Observable<string>;
  private subscriptions: Subscription = new Subscription();

  constructor(
    private store: Store<{ countryStore }>,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.selectedCountry$ = this.store.pipe(select(getSelectedCountry));

    // Get selectedCountry to check if empty in the store
    this.selectedCountrySub = this.selectedCountry$.subscribe(country => {
      this.selectedCountry = country;
    });

    // If selectedCountry is empty:
    if (this.selectedCountry.id === null) {

      // Get id from route
      this.idSub = this.route.params.subscribe(param => {
        this.id = param.id;
      });

      // Fetch all countries from API
      this.store.dispatch(new CountryActions.FetchCountriesStart());
      this.loading$ = this.store.pipe(select(getLoading));

      // If the country data is fetched and loaded set the APILoaded:
      this.APILoaded$ = this.store.pipe(select(getAPILoaded));
      this.APILoadedSub = this.APILoaded$.subscribe(APILoaded => {
        this.APILoaded = APILoaded;
        if (this.APILoaded) {
          // When loaded set the selectedCountry (equal to URL)
          this.store.dispatch(new CountryActions.SelectCountry(this.id));
        }
      });
    }

    this.subscriptions
    .add(this.idSub)
    .add(this.APILoadedSub)
    .add(this.selectedCountrySub);

  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  addCountry(countryId: string) {
    this.message$ = this.store.pipe(select(getMessage));
    this.store.dispatch(new CountryActions.AddToWishlist(countryId));
  }

}
