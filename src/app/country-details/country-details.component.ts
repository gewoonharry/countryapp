import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Country } from '../country.model';


@Component({
  selector: 'app-country-details',
  templateUrl: './country-details.component.html',
  styleUrls: ['./country-details.component.scss']
})
export class CountryDetailsComponent implements OnInit {
  @Input() selectedCountry: Country;
  @Input() loading: boolean;
  @Input() message: string;
  @Output() countryIdEmitter = new EventEmitter<string>();
  public isAddedCountry: boolean;

  constructor() {}

  ngOnInit() {
  }

  addCountry(countryId) {
    this.countryIdEmitter.emit(countryId);
    this.isAddedCountry = true;
  }

}
