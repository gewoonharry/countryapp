import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })

export class CountryService {

  constructor(
    private http: HttpClient
  ) {}

  private httpUrl = 'https://restcountries.eu/rest/v2/all';

  fetchCountries() {
    return this.http
      .get(this.httpUrl)
      .pipe(map(responseData => {
        const countriesArray = [];
        for (const key in responseData) {
          if (responseData.hasOwnProperty(key)) {
            countriesArray.push({ ...responseData[key], id: key });
          }
        }
        return countriesArray;
      })
    );
  }

}
