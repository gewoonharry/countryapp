import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CountryListContainerComponent } from './country-list/country-list.container';
import { CountryDetailsContainerComponent } from './country-details/country-details.container';
import { WishListContainerComponent } from './wish-list/wish-list.container';


const routes: Routes = [
  { path: '', redirectTo: 'country-list', pathMatch: 'full' },
  { path: 'country-list', component: CountryListContainerComponent },
  { path: 'country-details/:id', component: CountryDetailsContainerComponent },
  { path: 'wish-list', component: WishListContainerComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
