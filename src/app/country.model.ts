export class Country {
  id: string;
  name: string;
  region: string;
  population: number;
  flag: string;
}
