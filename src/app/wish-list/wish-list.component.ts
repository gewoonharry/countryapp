import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Country } from '../country.model';

@Component({
  selector: 'app-wish-list',
  templateUrl: './wish-list.component.html',
  styleUrls: ['./wish-list.component.scss']
})
export class WishListComponent implements OnInit {

  @Input() wishlist: Country[];
  @Output() removeCountry = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  onRemoveCountry(id) {
    this.removeCountry.emit(id);
  }

}
