import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { getWishlist } from '../store/index';
import { Country } from '../country.model';
import * as CountryActions from '../store/country.actions';

@Component({
  selector: 'app-wishlist-container',
  template: `
    <app-wish-list
      [wishlist]="wishlist$ | async"
      (removeCountry)="removeCountry($event)"
    >`
})

export class WishListContainerComponent implements OnInit {

  public wishlist$: Observable<Country[]>;

  constructor(private store: Store<{ countryStore }>) {}

  ngOnInit() {
    this.wishlist$ = this.store.pipe(select(getWishlist));
  }

  removeCountry(countryId) {
    this.store.dispatch(new CountryActions.RemoveFromWishlist(countryId));
  }

}
