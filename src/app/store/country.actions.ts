import { Action } from '@ngrx/store';

export const FETCH_COUNTRIES_START = '[COUNTRY-API] Load data start';
export const FETCH_COUNTRIES_SUCCES = '[COUNTRY-API] Load data succes';
export const FETCH_COUNTRIES_FAILED = '[COUNTRY-API] Load data failed';
export const SELECT_REGION = '[COUNTRY-LIST] Select region';
export const SELECT_COUNTRY = '[COUNTRY-DETAILS] Select country';
export const ADD_TO_WISHLIST = '[COUNTRY-DETAILS] Add country to wishlist';
export const REMOVE_FROM_WISHLIST = '[WISH-LIST] Remove from wishlist';

export class FetchCountriesStart implements Action {
  readonly type = FETCH_COUNTRIES_START;
}

export class FetchCountriesSucces implements Action {
  readonly type = FETCH_COUNTRIES_SUCCES;

  constructor(public payload: { data: any }) {}
}

export class FetchCountriesFailed implements Action {
  readonly type = FETCH_COUNTRIES_FAILED;

  constructor(public payload: { error: any }) {}
}

export class SelectRegion implements Action {
  readonly type = SELECT_REGION;

  constructor(public payload: string) {}
}

export class SelectCountry implements Action {
  readonly type = SELECT_COUNTRY;

  constructor(public payload: string) {}
}

export class AddToWishlist implements Action {
  readonly type = ADD_TO_WISHLIST;

  constructor(public payload: string) {}
}

export class RemoveFromWishlist implements Action {
  readonly type = REMOVE_FROM_WISHLIST;

  constructor(public payload: string) {}
}

export type CountryActions =
  | FetchCountriesStart
  | FetchCountriesSucces
  | FetchCountriesFailed
  | SelectRegion
  | SelectCountry
  | AddToWishlist
  | RemoveFromWishlist;

