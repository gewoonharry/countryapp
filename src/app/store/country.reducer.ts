import * as CountryActions from './country.actions';
import { Country } from '../country.model';

export interface CountryState {
  countryList: Country[];
  regions: string[];
  loading: boolean;
  error: any;
  APILoaded: boolean;
  selectedRegion: string;
  selectedCountry: Country;
  wishlist: Country[];
  message: string;
}

export const initialState: CountryState = {
  countryList: [],
  regions: [],
  loading: false,
  error: null,
  APILoaded: false,
  selectedRegion: 'All',
  selectedCountry: {
    id: null,
    name: null,
    region: null,
    population: null,
    flag: null
  },
  wishlist: [],
  message: null
};

export function CountryReducer(
  state = initialState,
  action: CountryActions.CountryActions): CountryState {
  switch (action.type) {

    case CountryActions.FETCH_COUNTRIES_START: {
      return {
        ...state,
        loading: true,
        error: null
      };
    }

    case CountryActions.FETCH_COUNTRIES_SUCCES: {
      // Set regions
      state.regions.push('All');
      action.payload.data.map(country => {
        // // Two countries with empty region. Rename to 'None'
        if (country.region === '') {
          country.region = 'None';
        }
        if (state.regions.indexOf(country.region) === -1) {
          state.regions.push(country.region);
        }
      });
      return {
        ...state,
        loading: false,
        countryList: action.payload.data,
        regions: state.regions,
        APILoaded: true
      };
    }

    case CountryActions.FETCH_COUNTRIES_FAILED: {
      return {
        ...state,
        loading: false,
        error: action.payload.error
      };
    }

    case CountryActions.SELECT_REGION: {
      return {
        ...state,
        selectedRegion: action.payload,
      };
    }

    case CountryActions.SELECT_COUNTRY: {
      state.countryList.map(country => {
        if (action.payload === country.id) {
          state.selectedCountry.id = country.id;
          state.selectedCountry.name = country.name;
          state.selectedCountry.region = country.region;
          state.selectedCountry.population = country.population;
          state.selectedCountry.flag = country.flag;
        }
      });
      return {
        ...state,
      };
    }

    case CountryActions.ADD_TO_WISHLIST: {
      let addItem = true;
      state.wishlist.map(list => {
        if (list.id === action.payload) {
          addItem = false;
          state.message = 'Country already added.';
        }
      });
      if (addItem) {
        const addCountry: Country = {
          id: null,
          name: null,
          region: null,
          population: null,
          flag: null
        };
        state.countryList.map(country => {
          if (action.payload === country.id) {
            addCountry.id = country.id;
            addCountry.name = country.name;
            addCountry.region = country.region;
            addCountry.population = country.population;
            addCountry.flag = country.flag;
          }
        });
        state.wishlist.push(addCountry);
        state.message = 'Country added.';
      }
      return {
        ...state,
      };
    }

    case CountryActions.REMOVE_FROM_WISHLIST: {
      const newWhishlist = state.wishlist.filter(item => item.id !== action.payload);
      return {
        ...state,
        wishlist: newWhishlist
      };
    }

    default:
      return state;
  }
}
