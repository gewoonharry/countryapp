import { createSelector, createFeatureSelector } from '@ngrx/store';
import { CountryState } from './country.reducer';
import { Country } from '../country.model';

export const getCountryState = createFeatureSelector<CountryState>('countryStore');
export const getCountries = createSelector(getCountryState, (state: CountryState) => {
  let countryFilter: Country[];
  if (state.selectedRegion === 'All') {
    countryFilter = state.countryList;
  } else {
    countryFilter = state.countryList.filter(country => {
      return country.region === state.selectedRegion;
    });
  }
  return countryFilter;
});

export const getLoading = createSelector(getCountryState, (state: CountryState) => state.loading);
export const getAPILoaded = createSelector(getCountryState, (state: CountryState) => state.APILoaded);
export const getRegions = createSelector(getCountryState, (state: CountryState) => state.regions);
export const getSelectedCountry = createSelector(getCountryState, (state: CountryState) => state.selectedCountry);
export const getSelectedRegion = createSelector(getCountryState, (state: CountryState) => state.selectedRegion);
export const getWishlist = createSelector(getCountryState, (state: CountryState) => state.wishlist);
export const getMessage = createSelector(getCountryState, (state: CountryState) => state.message);
