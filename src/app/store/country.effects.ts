import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';
import { CountryService } from '../country.service';
import * as CountryActions from './country.actions';

@Injectable()
export class CountryEffects {
  constructor(
    private actions: Actions,
    private countryService: CountryService
  ) {}

  @Effect()
  loadData = this.actions.pipe(
      ofType(CountryActions.FETCH_COUNTRIES_START),
      switchMap(() => {
        return this.countryService.fetchCountries()
          .pipe(
            map(data => new CountryActions.FetchCountriesSucces({ data })),
            catchError(error =>
              of (new CountryActions.FetchCountriesFailed({ error }))
            )
          );
      })
    );
  }

