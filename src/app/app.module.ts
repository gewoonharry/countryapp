import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { StoreModule } from '@ngrx/store';
import { CountryReducer} from './store/country.reducer';
import { EffectsModule } from '@ngrx/effects';
import { CountryEffects } from './store/country.effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { CountryListContainerComponent } from './country-list/country-list.container';
import { CountryListComponent } from './country-list/country-list.component';
import { CountryDetailsComponent } from './country-details/country-details.component';
import { CountryDetailsContainerComponent} from './country-details/country-details.container';
import { WishListComponent } from './wish-list/wish-list.component';
import { WishListContainerComponent } from './wish-list/wish-list.container';

@NgModule({
  declarations: [
    AppComponent,
    CountryListContainerComponent,
    CountryListComponent,
    CountryDetailsComponent,
    CountryDetailsContainerComponent,
    WishListComponent,
    WishListContainerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    StoreModule.forRoot({ countryStore: CountryReducer }),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
    EffectsModule.forRoot([CountryEffects]),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
