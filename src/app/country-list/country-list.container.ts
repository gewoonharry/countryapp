import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { getAPILoaded, getCountries, getLoading, getRegions, getSelectedRegion } from '../store/index';
import { Observable, Subscription } from 'rxjs';
import * as CountryActions from '../store/country.actions';
import { Country } from '../country.model';

@Component({
  selector: 'app-country-list-container',
  template: `
    <app-country-list
      [regions]="regions$ | async"
      [countries]="countries$ | async"
      [loading]="loading$ | async"
      [selectedRegion]="selectedRegion$ | async"
      (countryIdEmitter)="selectCountry($event)"
      (regionEmitter)="selectRegion($event)"
      >
    </app-country-list>
  `
})
export class CountryListContainerComponent implements OnInit, OnDestroy {

  public loading$: Observable<boolean>;
  public regions$: Observable<string[]>;
  public countries$: Observable<Country[]>;
  public selectedRegion$: Observable<string>;
  private APILoaded$: Observable<boolean>;
  private APILoadedSubscription: Subscription = new Subscription();
  private APILoaded: boolean;


  private selectCountry(countryId: string) {
    this.store.dispatch(new CountryActions.SelectCountry(countryId));
  }

  private selectRegion(region: string) {
    this.store.dispatch(new CountryActions.SelectRegion(region));
  }

  constructor(
    private store: Store<{ countryStore }>,
  ) {
  }

  ngOnInit() {
    this.APILoaded$ = this.store.pipe(select(getAPILoaded));
    this.APILoadedSubscription = this.APILoaded$.subscribe(APILoaded => {
      this.APILoaded = APILoaded;
    });
    if (!this.APILoaded) {
      this.store.dispatch(new CountryActions.FetchCountriesStart());
    }
    this.regions$ = this.store.pipe(select(getRegions));
    this.countries$ = this.store.pipe(select(getCountries));
    this.loading$ = this.store.pipe(select(getLoading));
    this.selectedRegion$ = this.store.pipe(select(getSelectedRegion));
  }

  ngOnDestroy() {
    this.APILoadedSubscription.unsubscribe();
  }

}
