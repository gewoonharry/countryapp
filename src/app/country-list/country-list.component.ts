import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Country } from '../country.model';

@Component({
  selector: 'app-country-list',
  templateUrl: './country-list.component.html',
  styleUrls: ['./country-list.component.scss']
})
export class CountryListComponent implements OnInit {

  @Input() countries: Country[];
  @Input() regions: string[];
  @Input() loading: boolean;
  @Input() selectedRegion: string;
  @Output() countryIdEmitter = new EventEmitter<string>();
  @Output() regionEmitter = new EventEmitter<string>();

  constructor() {
  }

  ngOnInit() {
  }

  onSelectCountry(id: string) {
    this.countryIdEmitter.emit(id);
  }

  onSelectRegion(region) {
    this.regionEmitter.emit(region);
  }

}
